/*--------------------------------------------------------------------
Name: C1C Mike Pieschl
Date: 08 OCT 17
Course: ECE 382
File: pong.h
Event: Assignment 7/Lab 4 - Pong

Purp: Implements a subset of the pong game

Doc:    <list the names of the people who you helped>
        <list the names of the people who assisted you>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef _PONG_H
#define _PONG_H

#define SCREEN_WIDTH 240
#define SCREEN_HEIGHT 320

#define		TRUE			1
#define		FALSE			0
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		UP_BUTTON		(P2IN & BIT2)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		START_BUTTON	(P1IN & BIT3)
#define		XSTART			115
#define		YSTART			155
#define		XVEL			4
#define		YVEL			4
#define		RADIUS			5
#define		COLOR1			0x4416
#define		COLORCLEAR		0x0000
#define		XPADDLE			100
#define		YPADDLE			310
#define		XVPADDLE		0
#define		YVPADDLE		0
#define		IR_PIN			(P2IN & BIT6)
#define		HIGH_2_LOW		P2IES |= BIT6
#define		LOW_2_HIGH		P2IES &= ~BIT6
#define		averageLogic0Pulse	0x0200
#define		averageLogic1Pulse	0x0645
#define		averageStartPulse	0x1100
#define		minLogic0Pulse		averageLogic0Pulse - 100
#define		maxLogic0Pulse		averageLogic0Pulse + 100
#define		minLogic1Pulse		averageLogic1Pulse - 100
#define		maxLogic1Pulse		averageLogic1Pulse + 100
#define		minStartPulse		averageStartPulse - 100
#define		maxStartPulse		averageStartPulse + 100
#define		ENTER	0x61A018E7
#define		LEFT	0x61A06897
#define		RIGHT	0x61A0A857
#define		HOLD	0x1
typedef unsigned char 		int8;
typedef char 				sint8;
typedef unsigned short 		int16;
typedef short 				sint16;
typedef unsigned long 		int32;
typedef long 				sint32;
typedef unsigned long long 	int64;
typedef long long 			sint64;

extern void initMSP();
extern void initLCD();
extern void clearScreen();
extern void drawBox(sint16 col, sint16 row, int8 radius, int16 color);
extern void Delay160ms();
extern void Delay40ms();

typedef struct {
	sint16 x;
	sint16 y;
} vector2d_t;

typedef struct {
	vector2d_t position;
	vector2d_t velocity;
	int8 radius;
	int16 color;
} ball_t;

ball_t createBall(sint16 xPos, sint16 yPos, sint8 xVel, sint8 yVel, int8 rad, int16 color);
ball_t moveBall(ball_t ballToMove, ball_t paddleCheck);
ball_t movePaddle(ball_t paddleToMove);
int8 collisionDetectionBall(ball_t checkBall, ball_t checkPaddle);
int8 collisionDetectionPaddle(ball_t checkPaddle);
void drawPaddle(ball_t paddleBalls);
void initRemote();
__interrupt void pinChange (void);
__interrupt void timerOverflow (void);

#endif
