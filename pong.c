/*--------------------------------------------------------------------
Name: C1C Mike Pieschl
Date: 08 OCT 17
Course: ECE 382
File: pong.c
Event: Assignment 7/Lab 4 - Pong

Purp: Contains the functions addressed in pong.h

Doc:    <list the names of the people who you helped>
        <list the names of the people who assisted you>

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430g2553.h>
#include "pong.h"

ball_t createBall(sint16 xPos, sint16 yPos, sint8 xVel, sint8 yVel, int8 rad, int16 color){
	vector2d_t spotBall;
	spotBall.x = xPos;
	spotBall.y = yPos;
	vector2d_t throwBall;
	throwBall.x = xVel;
	throwBall.y = yVel;
	ball_t setBall;
	setBall.position = spotBall;
	setBall.velocity = throwBall;
	setBall.radius = rad;
	setBall.color = color;

	return setBall;
}

ball_t moveBall(ball_t ballToMove, ball_t paddleCheck){

	int8 collisionStatus = collisionDetectionBall(ballToMove, paddleCheck);

	if(collisionStatus == 1){
		ballToMove.velocity.x = -ballToMove.velocity.x;
	}
	else if(collisionStatus == 2){
		ballToMove.velocity.y = -ballToMove.velocity.y;
	}
	else if(collisionStatus == 3){
		ballToMove.velocity.y = -ballToMove.velocity.y;
	}
	else if(collisionStatus == 4){
		ballToMove.velocity.x = 0;
		ballToMove.velocity.y = 0;
	}
	else{;}

	ballToMove.position.x += ballToMove.velocity.x;
	ballToMove.position.y += ballToMove.velocity.y;

	return ballToMove;
}

ball_t movePaddle(ball_t paddleToMove){

	int8 collisionStatus = collisionDetectionPaddle(paddleToMove);

	if(collisionStatus == 1){
		paddleToMove.position.x += -paddleToMove.velocity.x;
	}
	else{
		paddleToMove.position.x += paddleToMove.velocity.x;
	}

	return paddleToMove;
}

int8 collisionDetectionBall(ball_t checkBall, ball_t checkPaddle){

	int8 collisionStatus;

	if((checkBall.position.x <= 0)||(checkBall.position.x >= (SCREEN_WIDTH - 2*RADIUS))){
		collisionStatus =  1;
	}
	else if((checkBall.position.y <= 0)){
		collisionStatus = 2;
	}
	else if((checkBall.position.y >= SCREEN_HEIGHT - 4*checkPaddle.radius)&&(checkBall.position.x >= checkPaddle.position.x - 2*checkPaddle.radius)&&(checkBall.position.x <= checkPaddle.position.x + 6*checkPaddle.radius)){
		collisionStatus = 3;
		drawPaddle(checkPaddle);
	}
	else if(checkBall.position.y >= (SCREEN_HEIGHT - 2*RADIUS)){
		collisionStatus = 4;
	}
	else{
		collisionStatus = 0;
	}

	return collisionStatus;
}

int8 collisionDetectionPaddle(ball_t checkPaddle){

	int8 collisionStatus;

	if(checkPaddle.position.x <= 0||checkPaddle.position.x >= (SCREEN_WIDTH - 6*RADIUS)){
		collisionStatus =  1;
	}
	else{
		collisionStatus = 0;
	}

	return collisionStatus;
}

void drawPaddle(ball_t paddleBalls){
	drawBox(paddleBalls.position.x, paddleBalls.position.y, paddleBalls.radius, paddleBalls.color);
	drawBox(paddleBalls.position.x + 2*paddleBalls.radius, paddleBalls.position.y, paddleBalls.radius, paddleBalls.color);
	drawBox(paddleBalls.position.x + 4*paddleBalls.radius, paddleBalls.position.y, paddleBalls.radius, paddleBalls.color);
}

void initRemote(){
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2DIR &= ~BIT6;						// Set up P2.6 as GPIO not XIN
	P2SEL &= ~BIT6;						// Once again, this take three lines
	P2SEL2 &= ~BIT6;						// to properly do

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	HIGH_2_LOW;							// check the header out.  P2IES changed.
	P1DIR |= BIT0|BIT6;                 // set LEDs to output
	P1OUT &= ~(BIT0|BIT6);				// And turn the LEDs off

	TA0CCR0 = 16000-1;					// create a 16ms roll-over period
	TACTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TACTL = ID_3|TASSEL_2|MC_1;			// Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();
}
