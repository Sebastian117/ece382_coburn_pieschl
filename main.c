#include <msp430.h> 
#include "pong.h"

int8	newIrPacket = FALSE;
int32	irPacket;
int32 	lastIrPacket;

void main() {

	//-------------Initialize the MSP430-----------------

	IFG1 = 0;  // Clear Interrupt Flag
    WDTCTL = WDTPW | WDTHOLD;	// Stop Watch Dog Timer

    initMSP();
    Delay160ms();
    initLCD();
    Delay160ms();
    clearScreen();
    Delay160ms();
    initRemote();

    //----------------------------------------------------

    //--------------Initialize Variables------------------

    int8 go = 0;

    //----------------------------------------------------

    //------Initialize the Ball, Paddle, and Score--------

    ball_t readyBall = createBall(XSTART, YSTART, XVEL, YVEL, RADIUS, COLOR1);
    ball_t readyPaddle = createBall(XPADDLE, YPADDLE, XVPADDLE, YVPADDLE, RADIUS, COLOR1);
    drawBox(readyBall.position.x, readyBall.position.y, readyBall.radius, readyBall.color);
    drawPaddle(readyPaddle);

    //----------------------------------------------------

    while(1){
    	// Main Loop
    	if(irPacket == ENTER){
    		// Start Button - Enter
    		Delay160ms();
    		go = 1;
    		irPacket = 0;
    	}
    	else{;}

    	while(go != 0){
    		Delay40ms(); // Controls the speed of the pong game

    		drawBox(readyBall.position.x, readyBall.position.y, readyBall.radius, COLORCLEAR);
    		readyBall = moveBall(readyBall, readyPaddle);
    		drawBox(readyBall.position.x, readyBall.position.y, readyBall.radius, readyBall.color);

    		if((readyBall.velocity.x == 0)&&(readyBall.velocity.y == 0)){
    			// Resets the game -- "Game Over"
    			Delay160ms();
    			Delay160ms();
    			Delay160ms();
    			Delay160ms();
    			go = 0;
    			readyPaddle.color = COLORCLEAR;
    			drawPaddle(readyPaddle);
    			readyBall.color = COLORCLEAR;
    			drawBox(readyBall.position.x, readyBall.position.y, readyBall.radius, readyBall.color);
    			readyBall = createBall(XSTART, YSTART, XVEL, YVEL, RADIUS, COLOR1);
    			readyPaddle = createBall(XPADDLE, YPADDLE, XVPADDLE, YVPADDLE, RADIUS, COLOR1);
    			drawBox(readyBall.position.x, readyBall.position.y, readyBall.radius, readyBall.color);
    			drawPaddle(readyPaddle);
    		}
    		if(irPacket == RIGHT){
    			// Moves the Paddle Right
   				readyPaddle.color = COLORCLEAR;
   				drawPaddle(readyPaddle);
   				readyPaddle.color = COLOR1;
   		    	readyPaddle.velocity.x = 7;
   		    	readyPaddle = movePaddle(readyPaddle);
   		    	drawPaddle(readyPaddle);
    		}
    		if(irPacket == LEFT){
    			// Moves the Paddle Left
    		    readyPaddle.color = COLORCLEAR;
    		    drawPaddle(readyPaddle);
    		    readyPaddle.color = COLOR1;
    		    readyPaddle.velocity.x = -7;
    		    readyPaddle = movePaddle(readyPaddle);
    		    drawPaddle(readyPaddle);
    		}
    	}
    }
}

#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file
__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;	//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if((minLogic1Pulse <= pulseDuration)&&(maxLogic1Pulse >= pulseDuration))
				irPacket = (irPacket<<1) | 1;
			if((minLogic0Pulse <= pulseDuration)&&(maxLogic0Pulse >= pulseDuration))
				irPacket = (irPacket<<1) | 0;
			TA0CTL = 0;			// enable interrupt
			LOW_2_HIGH; 			// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CCR0 = 0x2710;
			TA0CTL = ID_3|TASSEL_2|MC_1|TAIE;          		// set count mode to continuous|enable interrupt
			HIGH_2_LOW; 						// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {
	TACTL = 0;
	TACTL ^= TAIE;
	newIrPacket = TRUE;
	TA0CTL &= ~TAIFG;
}
